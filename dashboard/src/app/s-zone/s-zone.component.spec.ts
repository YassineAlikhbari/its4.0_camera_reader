import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SZoneComponent } from './s-zone.component';

describe('SZoneComponent', () => {
  let component: SZoneComponent;
  let fixture: ComponentFixture<SZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
