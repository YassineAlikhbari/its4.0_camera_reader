# import the necessary packages
from pyzbar import pyzbar
import cv2
from controllers.geometry import Geometry
from models.qr_info import QRInfo
from models.rect import Rect
from db import influx, mongo
import json
import pytz
from datetime import datetime
from controllers.receipt import Receipt
from models.zone import Zone


def newqr(info: QRInfo):
    z: Zone = Geometry.rect_to_zone(info.rect, perimeter.zones)
    info.zone_id = z.id

    print(f"carrello con Id = {info.qr_id} rilevato nella zona '{z.name}' - id = {z.id}")
    if z.name == "e":
        print(f"   la zona non è calpestabile")

    print(f"{info.rect.top},{info.rect.left}       Top, Left      {z.rect.top},{z.rect.left}")
    print(
        f"{info.rect.top + info.rect.height},{info.rect.left + info.rect.width}   "
        f" Bottom, Right    {z.rect.top + z.rect.height},{z.rect.left + z.rect.width}")


def generate_timestamp():
    # timestamp manager
    rome = pytz.timezone("Europe/Rome")
    utc = datetime.utcnow()
    # obj_time = utc.astimezone(rome)
    # obj_time = obj_time.replace(tzinfo=pytz.utc, microsecond=0)
    return utc.isoformat("T") + "Z"


inf_client = influx.client

perimeter = Geometry.load_perimeter("test.json")

# initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")

cap = cv2.VideoCapture(0)  # use webcam as source

# cap = cv2.VideoCapture(1)  # use ext camera as source

found = set()  # known qrcode id
known_cart_list = []  # store "new" cart entered to compare in-out of cart from market

while (True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Display the resulting frame
    cv2.imshow('Barcode reader', frame)
    barcodes = pyzbar.decode(frame)  # decode  qr_codes

    for barcode in barcodes:
        barcodeType = barcode.type

        rect_bar1 = Rect(barcode.rect.left, barcode.rect.top, barcode.rect.width, barcode.rect.height)
        rect_bar2 = Rect(barcode.rect.left, barcode.rect.top, barcode.rect.width, barcode.rect.height)

        barcodeData = barcode.data.decode("utf-8")  # decode qr models
        barcodeData_json = json.loads(barcodeData)  # transform string to dict

        # qr code creation
        qr = QRInfo()
        qr.rect = Rect(left=barcode.rect.left, top=barcode.rect.top, width=barcode.rect.width,
                       height=barcode.rect.height)
        qr.qr_id = barcodeData_json["cart_id"]
        newqr(qr)

        list_barcode = {
            "cart_id": barcodeData_json["cart_id"],
            "timestamp": generate_timestamp()
        }

        if barcodeData not in found:
            found.add(barcodeData)
            known_cart_list.append(list_barcode)
            print(list_barcode["timestamp"])

            # show the output frame
            cv2.imshow("Barcode reader", frame)

        # influx dato
        json_body = [
            {
                "measurement": "cart_data",
                "tags": {
                    "supermarket_name": "its_market"

                },
                "fields": {
                    "cart_id": barcodeData_json["cart_id"],
                    "zone_id": qr.zone_id
                },
                "time": generate_timestamp()
            }
        ]

        inf_client.write_points(json_body)

        print("Written on influx.")

        (x, y, w, h) = barcode.rect
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
        # draw the barcode models and barcode type on the image
        text = "{} ({})".format(barcodeData, barcodeType)
        cv2.putText(frame, text, (x, y - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

        '''
         res = inf_client.query(
            "SELECT cart_id, zone_id FROM qr_codes.autogen.cart_data WHERE time >= '2019-06-11T11:54:17Z' "
            "AND time <= '2019-06-11T19:10:27Z' AND cart_id = 3")
        print(res)
        
        query_params = {"et": entered_timestamp, "ex": exited_timestamp, "cart": cart_id}
        res = inf_client.query(
                "SELECT cart_id, zone_id FROM qr_codes.autogen.cart_data WHERE time >= $et "
                "AND time <= $ex AND cart_id =$cart",
                params={"params": json.dumps(query_params)})
        '''

        # 16 ipotetica uscita
        if barcodeData in found and qr.zone_id == 16:

            initial_cart_value = {}
            for idx, known in enumerate(known_cart_list):
                if known["cart_id"] == barcodeData_json["cart_id"]:
                    initial_cart_value = known
                    known_cart_list.remove(known)

            cart_id, entered_timestamp = initial_cart_value['cart_id'], initial_cart_value['timestamp']
            exited_timestamp = generate_timestamp()
            print(f"Entered timestamp: {initial_cart_value['timestamp']}")
            print(f"Exited timestamp: {exited_timestamp}")

            query_params = {"et": entered_timestamp, "ex": exited_timestamp, "cart": cart_id}
            res = inf_client.query(
                "SELECT cart_id, zone_id FROM qr_codes.autogen.cart_data WHERE time >= $et "
                "AND time <= $ex AND cart_id =$cart",
                params={"params": json.dumps(query_params)})

            # loop over res return a list, so re-loop over result
            zone_list = []
            for query_list in res:
                for idx, data in enumerate(query_list):
                    if data['zone_id'] != 16 and query_list[idx]['zone_id'] != query_list[idx - 1]['zone_id']:
                        zone_list.append(data['zone_id'])

            if zone_list:
                print(f'cart_id: {cart_id} \t zone: {zone_list} \t exit_time: {exited_timestamp}')
                cart_receipt = Receipt.create_receipt(cart_id, zone_list, exited_timestamp)
                mongo.insert_document(cart_receipt)

            # TODO
            #   avere arco di entrata e uscita carrello (salvare in memoria entrata e uscita)
            #   fare query per avere le zone nel range di entrata e uscita
            #   avere lista delle zone (zone_id) e carrello_id  e passarle a funzione per creare scontrino
            #   SALVARE SCONTRINO IN DB MONGO (mLab)
            print(f"Qrcode {barcodeData} uscito da zona {qr.zone_id}")
            found.remove(barcodeData)

    # quit app
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()



'''
Inizialmente il qr_code rilevato "è nuovo". Viene inserito in una lista
che verrà verificata quando il qr_code passa dal gate finale (cassa)
Si suppone quindi che il carrello sia uscito dal supermercato e viene cancellato dalla lista precedente.

L'oggetto salvato in questa lista contiene un oggetto con:
- id_carrello
- timestamp

In questo modo riesco a fare la differenza tra timestamp di ingresso e uscita
del carrello. 
Faccio la differenza e so il tempo di permanenza nel centro così da fare la query su influx e acquisire le zone di passaggio


'''

