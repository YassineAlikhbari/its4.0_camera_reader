import time
import random


class Receipt:
    def __init__(self):
        pass

    @staticmethod
    def create_receipt(cart_id: int, zone_list: [int], exit_cart_time: time):
        shopping_list = []
        total_spending = 0

        for idx, zone in enumerate(zone_list):
            random_shopping_amount = round(random.randint(15, 1000) * 60) / 1000

            if random_shopping_amount != 0:
                shopping_item = {
                    "zone_id": zone_list[idx],
                    "shop_amount": random_shopping_amount
                }
                total_spending += random_shopping_amount
                shopping_list.append(shopping_item)

            total_spending = round(total_spending * 100) / 100

        output_receipt = {
            "cart_id": cart_id,
            "shopping_list": shopping_list,
            "total_spending": total_spending,
            "timestamp": exit_cart_time
        }

        return output_receipt

